﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Leadership : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GetLeadershipDetails();
    }
    
    protected void GetLeadershipDetails()
    {
        ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["aitp"];
        SqlConnection con = null;
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        SqlCommand cmd = null;
        SqlParameter sp1 = new SqlParameter();

        string conString = connectionStringSettings.ConnectionString;
        using (con = new SqlConnection(conString))
        {
            try
            {
                cmd = new SqlCommand("GetActiveOfficers", con);
                cmd.Parameters.Add("@isActive", SqlDbType.Bit).Value = true;
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();         
                da.SelectCommand = cmd;
                da.Fill(ds, "Table");
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }
    }
}