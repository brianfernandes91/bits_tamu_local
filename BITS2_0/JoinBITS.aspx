﻿<%@ Page Language="C#"  MasterPageFile="~/BITSMaster.master" AutoEventWireup="true" CodeFile="JoinBITS.aspx.cs" Inherits="JoinBITS" %>

<asp:Content ID="faqsContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="center">
            <br />
            <h2>BITS Membership</h2>
            <h4> To become a member of BITS: </h4>       
            <h3>1. Download the Membership Application
                <a href="documents/BITS Student Membership Application.pdf" target="_new">
                    <img src="images/pdf_file.png" title="BITS Student Membership Application" style="height:30px; width:30px; margin-top:10px;">
                </a>
            </h3>
            <h3>2. Fill in the details</h3>
            <h3>3. Submit it along with the membership dues to any of the BITS officers at our weekly meetings</h3>
            <br />
            <br />
            <h4>To see the venue and dates of our weekly meetings, refer to the <a href="Home.aspx">Events</a> page</h4>
        </div>
    </div>
</asp:Content>
