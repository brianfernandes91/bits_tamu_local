﻿<%@ Page Language="C#" MasterPageFile="~/BITSMaster.master" AutoEventWireup="true" CodeFile="FAQs.aspx.cs" Inherits="FAQs" %>

<asp:Content ID="faqsContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="center">
            <br />
            <h2>Frequently Asked Questions</h2>
        </div>
        <div class="accordion">
            <div class="panel-group" id="accordion1">
                <div class="panel panel-default">
                    <div class="panel-heading ">
                    <h3 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">
                        What is the purpose of BITS?
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    </h3>
                </div>
                    <div id="collapseOne1" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="media accordion-inner">
                            <div class="pull-left">
                                <img class="img-responsive" src="images/accordion1.png">
                            </div>
                            <div class="media-body">
                                <h4>Purpose of BITS</h4>
                                <p>The purpose of BITS is to provide a vehicle for its members to further their
                                    knowledge of the IT and business fields by providing leadership development 
                                    opportunities, networking, informational sessions, field trips, and seminars, 
                                    leading to better careers.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                    <h3 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1">
                        How will I benefit from BITS?
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    </h3>
                </div>
                    <div id="collapseTwo1" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row">
                            <h4>Information</h4>
                            <p>BITS's agenda supplements your education at A&M nicely through activities such as Company Visits, Tours, and Seminars. </p>
                        </div>
                        <div class="row">
                            <h4>Involvement</h4>
                            <p>With the current job market so competitive, companies want to see applicants who do more than just attend class and receive grades. They want to see aggressive people who get involved in their careers. BITS provides a way for you to show this participation to companies you are interested in. </p>
                        </div>
                        <div class="row">
                            <h4>Networking</h4>
                            <p>BITS provides a way to network with other IS students, professionals in the field, and an often overlooked resource- faculty. The network of people you build can benefit you throughout your career. The best jobs are not advertised but come through a combination of exposure and persistence. BITS cannot provide you with persistence, but we can expose you to local and national professionals. In addition, as a member, you receive a host of benefits (see question below). </p>
                        </div>
                    </div>
                </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree1">
                            How do I become a member and what is the membership fee?
                            <i class="fa fa-angle-right pull-right"></i>
                        </a>
                        </h3>
                    </div>
                    <div id="collapseThree1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>
                                BITS provides a way to network with other IS students, professionals in the field, and an often overlooked resource- faculty. 
                                <br />The network of people you build can benefit you throughout your career. The best jobs are not advertised but come through a combination of exposure and persistence. 
                                BITS cannot provide you with persistence, but we can expose you to local and national professionals. 
                                <br />In addition, as a member, you receive a host of benefits (see question below).
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour1">
                            What are the specific benefits of BITS membership?
                            <i class="fa fa-angle-right pull-right"></i>
                        </a>
                        </h3>
                    </div>
                    <div id="collapseFour1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>
                                In joining BITS, you become part of a strong network of IT business professionals, have access to empowering resources and opportunities, and are positioned to achieve your professional goals. As a member of BITS you will be entitled to following benefits:
                            </p>
                            <ul>
                                <li>Two field trips</li>
                                <li>Socials</li>
                                <li>Inclusion of your resume in resume book (distributed during business career fairs) </li>
                                <li>T-shirt </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive1">
                            What is appropriate to wear to BITS meetings?
                            <i class="fa fa-angle-right pull-right"></i>
                        </a>
                        </h3>
                    </div>
                    <div id="collapseFive1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>
                                <b>All BITS meetings are business casual. </b>
                                <br />You may choose to be in complete formals, but jeans/cargos/t-shirts are not appropriate.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseSix1">
                           Who can I contact if I have more questions
                            <i class="fa fa-angle-right pull-right"></i>
                        </a>
                        </h3>
                    </div>
                    <div id="collapseSix1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>
                                You can contact any BITS officer, or you can direct your questions to our email address, bits.tamu@gmail.com. 
                                <br />You can also get in touch with us via Facebook, Twitter or LinkedIn.
                            </p>
                        </div>
                    </div>
                </div>
            </div><!--/#accordion1-->
        </div>
    </div>
    <br />
</asp:Content>
